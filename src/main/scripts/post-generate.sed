#
# post-generate.sed
#
# vi: ft=sed :
#

# Escape '<' and '>' in Javadoc comments (Java 1.9+)
/^\s+\*\s+&lt;/ {
	s/>\s*$/\&gt;/g;
}
