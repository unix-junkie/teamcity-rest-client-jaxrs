
package org.jetbrains.teamcity.rest.client.jaxrs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sessions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sessions"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}session" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="count" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="maxActive" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="sessionCounter" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="sessionCreateRate" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="sessionExpireRate" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="sessionMaxAliveTime" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sessions", propOrder = {
    "session"
})
public class Sessions {

    protected List<Session> session;
    @XmlAttribute(name = "count")
    protected Integer count;
    @XmlAttribute(name = "maxActive")
    protected Integer maxActive;
    @XmlAttribute(name = "sessionCounter")
    protected Integer sessionCounter;
    @XmlAttribute(name = "sessionCreateRate")
    protected Integer sessionCreateRate;
    @XmlAttribute(name = "sessionExpireRate")
    protected Integer sessionExpireRate;
    @XmlAttribute(name = "sessionMaxAliveTime")
    protected Integer sessionMaxAliveTime;

    /**
     * Gets the value of the session property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the session property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSession().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Session }
     * 
     * 
     */
    public List<Session> getSession() {
        if (session == null) {
            session = new ArrayList<Session>();
        }
        return this.session;
    }

    /**
     * Gets the value of the count property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCount(Integer value) {
        this.count = value;
    }

    /**
     * Gets the value of the maxActive property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxActive() {
        return maxActive;
    }

    /**
     * Sets the value of the maxActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxActive(Integer value) {
        this.maxActive = value;
    }

    /**
     * Gets the value of the sessionCounter property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSessionCounter() {
        return sessionCounter;
    }

    /**
     * Sets the value of the sessionCounter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSessionCounter(Integer value) {
        this.sessionCounter = value;
    }

    /**
     * Gets the value of the sessionCreateRate property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSessionCreateRate() {
        return sessionCreateRate;
    }

    /**
     * Sets the value of the sessionCreateRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSessionCreateRate(Integer value) {
        this.sessionCreateRate = value;
    }

    /**
     * Gets the value of the sessionExpireRate property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSessionExpireRate() {
        return sessionExpireRate;
    }

    /**
     * Sets the value of the sessionExpireRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSessionExpireRate(Integer value) {
        this.sessionExpireRate = value;
    }

    /**
     * Gets the value of the sessionMaxAliveTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSessionMaxAliveTime() {
        return sessionMaxAliveTime;
    }

    /**
     * Sets the value of the sessionMaxAliveTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSessionMaxAliveTime(Integer value) {
        this.sessionMaxAliveTime = value;
    }

}
