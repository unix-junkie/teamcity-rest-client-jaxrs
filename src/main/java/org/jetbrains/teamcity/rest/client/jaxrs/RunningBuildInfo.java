
package org.jetbrains.teamcity.rest.client.jaxrs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for runningBuildInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="runningBuildInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="outdatedReasonBuild" type="{}build" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="percentageComplete" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="elapsedSeconds" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="estimatedTotalSeconds" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="leftSeconds" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="currentStageText" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="outdated" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="probablyHanging" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="lastActivityTime" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "runningBuildInfo", propOrder = {
    "outdatedReasonBuild"
})
public class RunningBuildInfo {

    protected Build outdatedReasonBuild;
    @XmlAttribute(name = "percentageComplete")
    protected Integer percentageComplete;
    @XmlAttribute(name = "elapsedSeconds")
    protected Long elapsedSeconds;
    @XmlAttribute(name = "estimatedTotalSeconds")
    protected Long estimatedTotalSeconds;
    @XmlAttribute(name = "leftSeconds")
    protected Long leftSeconds;
    @XmlAttribute(name = "currentStageText")
    protected String currentStageText;
    @XmlAttribute(name = "outdated")
    protected Boolean outdated;
    @XmlAttribute(name = "probablyHanging")
    protected Boolean probablyHanging;
    @XmlAttribute(name = "lastActivityTime")
    protected String lastActivityTime;

    /**
     * Gets the value of the outdatedReasonBuild property.
     * 
     * @return
     *     possible object is
     *     {@link Build }
     *     
     */
    public Build getOutdatedReasonBuild() {
        return outdatedReasonBuild;
    }

    /**
     * Sets the value of the outdatedReasonBuild property.
     * 
     * @param value
     *     allowed object is
     *     {@link Build }
     *     
     */
    public void setOutdatedReasonBuild(Build value) {
        this.outdatedReasonBuild = value;
    }

    /**
     * Gets the value of the percentageComplete property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPercentageComplete() {
        return percentageComplete;
    }

    /**
     * Sets the value of the percentageComplete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPercentageComplete(Integer value) {
        this.percentageComplete = value;
    }

    /**
     * Gets the value of the elapsedSeconds property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getElapsedSeconds() {
        return elapsedSeconds;
    }

    /**
     * Sets the value of the elapsedSeconds property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setElapsedSeconds(Long value) {
        this.elapsedSeconds = value;
    }

    /**
     * Gets the value of the estimatedTotalSeconds property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEstimatedTotalSeconds() {
        return estimatedTotalSeconds;
    }

    /**
     * Sets the value of the estimatedTotalSeconds property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEstimatedTotalSeconds(Long value) {
        this.estimatedTotalSeconds = value;
    }

    /**
     * Gets the value of the leftSeconds property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLeftSeconds() {
        return leftSeconds;
    }

    /**
     * Sets the value of the leftSeconds property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLeftSeconds(Long value) {
        this.leftSeconds = value;
    }

    /**
     * Gets the value of the currentStageText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentStageText() {
        return currentStageText;
    }

    /**
     * Sets the value of the currentStageText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentStageText(String value) {
        this.currentStageText = value;
    }

    /**
     * Gets the value of the outdated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOutdated() {
        return outdated;
    }

    /**
     * Sets the value of the outdated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOutdated(Boolean value) {
        this.outdated = value;
    }

    /**
     * Gets the value of the probablyHanging property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProbablyHanging() {
        return probablyHanging;
    }

    /**
     * Sets the value of the probablyHanging property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProbablyHanging(Boolean value) {
        this.probablyHanging = value;
    }

    /**
     * Gets the value of the lastActivityTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastActivityTime() {
        return lastActivityTime;
    }

    /**
     * Sets the value of the lastActivityTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastActivityTime(String value) {
        this.lastActivityTime = value;
    }

}
