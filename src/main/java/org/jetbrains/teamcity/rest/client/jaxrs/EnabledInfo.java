
package org.jetbrains.teamcity.rest.client.jaxrs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enabledInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enabledInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}booleanStatus"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="statusSwitchTime" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enabledInfo")
public class EnabledInfo
    extends BooleanStatus
{

    @XmlAttribute(name = "statusSwitchTime")
    protected String statusSwitchTime;

    /**
     * Gets the value of the statusSwitchTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusSwitchTime() {
        return statusSwitchTime;
    }

    /**
     * Sets the value of the statusSwitchTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusSwitchTime(String value) {
        this.statusSwitchTime = value;
    }

}
