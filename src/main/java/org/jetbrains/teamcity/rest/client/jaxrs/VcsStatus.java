
package org.jetbrains.teamcity.rest.client.jaxrs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for vcsStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="vcsStatus"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="current" type="{}VcsCheckStatus" minOccurs="0"/&gt;
 *         &lt;element name="previous" type="{}VcsCheckStatus" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "vcsStatus", propOrder = {
    "current",
    "previous"
})
public class VcsStatus {

    protected VcsCheckStatus current;
    protected VcsCheckStatus previous;

    /**
     * Gets the value of the current property.
     * 
     * @return
     *     possible object is
     *     {@link VcsCheckStatus }
     *     
     */
    public VcsCheckStatus getCurrent() {
        return current;
    }

    /**
     * Sets the value of the current property.
     * 
     * @param value
     *     allowed object is
     *     {@link VcsCheckStatus }
     *     
     */
    public void setCurrent(VcsCheckStatus value) {
        this.current = value;
    }

    /**
     * Gets the value of the previous property.
     * 
     * @return
     *     possible object is
     *     {@link VcsCheckStatus }
     *     
     */
    public VcsCheckStatus getPrevious() {
        return previous;
    }

    /**
     * Sets the value of the previous property.
     * 
     * @param value
     *     allowed object is
     *     {@link VcsCheckStatus }
     *     
     */
    public void setPrevious(VcsCheckStatus value) {
        this.previous = value;
    }

}
