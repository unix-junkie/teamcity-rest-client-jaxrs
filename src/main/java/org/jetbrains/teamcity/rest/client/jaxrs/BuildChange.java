
package org.jetbrains.teamcity.rest.client.jaxrs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for buildChange complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="buildChange"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nextBuild" type="{}build" minOccurs="0"/&gt;
 *         &lt;element name="prevBuild" type="{}build" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "buildChange", propOrder = {
    "nextBuild",
    "prevBuild"
})
public class BuildChange {

    protected Build nextBuild;
    protected Build prevBuild;

    /**
     * Gets the value of the nextBuild property.
     * 
     * @return
     *     possible object is
     *     {@link Build }
     *     
     */
    public Build getNextBuild() {
        return nextBuild;
    }

    /**
     * Sets the value of the nextBuild property.
     * 
     * @param value
     *     allowed object is
     *     {@link Build }
     *     
     */
    public void setNextBuild(Build value) {
        this.nextBuild = value;
    }

    /**
     * Gets the value of the prevBuild property.
     * 
     * @return
     *     possible object is
     *     {@link Build }
     *     
     */
    public Build getPrevBuild() {
        return prevBuild;
    }

    /**
     * Sets the value of the prevBuild property.
     * 
     * @param value
     *     allowed object is
     *     {@link Build }
     *     
     */
    public void setPrevBuild(Build value) {
        this.prevBuild = value;
    }

}
