
package org.jetbrains.teamcity.rest.client.jaxrs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.jetbrains.teamcity.rest.client.jaxrs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MetricTag_QNAME = new QName("", "metricTag");
    private final static QName _Projects_QNAME = new QName("", "projects");
    private final static QName _Plugins_QNAME = new QName("", "plugins");
    private final static QName _TestOccurrence_QNAME = new QName("", "testOccurrence");
    private final static QName _AuditEvent_QNAME = new QName("", "auditEvent");
    private final static QName _Type_QNAME = new QName("", "type");
    private final static QName _Branch_QNAME = new QName("", "branch");
    private final static QName _Issues_QNAME = new QName("", "issues");
    private final static QName _CloudProfiles_QNAME = new QName("", "cloudProfiles");
    private final static QName _LicenseKeys_QNAME = new QName("", "licenseKeys");
    private final static QName _Problem_QNAME = new QName("", "problem");
    private final static QName _Builds_QNAME = new QName("", "builds");
    private final static QName _Property_QNAME = new QName("", "property");
    private final static QName _Href_QNAME = new QName("", "href");
    private final static QName _Sessions_QNAME = new QName("", "sessions");
    private final static QName _ArtifactDependencies_QNAME = new QName("", "artifact-dependencies");
    private final static QName _VcsRoot_QNAME = new QName("", "vcs-root");
    private final static QName _Mute_QNAME = new QName("", "mute");
    private final static QName _Triggers_QNAME = new QName("", "triggers");
    private final static QName _Tags_QNAME = new QName("", "tags");
    private final static QName _AgentRequirements_QNAME = new QName("", "agent-requirements");
    private final static QName _VcsRootEntries_QNAME = new QName("", "vcs-root-entries");
    private final static QName _Metrics_QNAME = new QName("", "metrics");
    private final static QName _Items_QNAME = new QName("", "items");
    private final static QName _ProjectFeatures_QNAME = new QName("", "projectFeatures");
    private final static QName _Server_QNAME = new QName("", "server");
    private final static QName _Role_QNAME = new QName("", "role");
    private final static QName _VcsRootEntry_QNAME = new QName("", "vcs-root-entry");
    private final static QName _MetricValue_QNAME = new QName("", "metricValue");
    private final static QName _Mutes_QNAME = new QName("", "mutes");
    private final static QName _BranchVersion_QNAME = new QName("", "branchVersion");
    private final static QName _MetaData_QNAME = new QName("", "metaData");
    private final static QName _File_QNAME = new QName("", "file");
    private final static QName _Feature_QNAME = new QName("", "feature");
    private final static QName _VcsLabeling_QNAME = new QName("", "vcsLabeling");
    private final static QName _AgentRequirement_QNAME = new QName("", "agent-requirement");
    private final static QName _AgentPool_QNAME = new QName("", "agentPool");
    private final static QName _AuthorizedInfo_QNAME = new QName("", "authorizedInfo");
    private final static QName _OperationResult_QNAME = new QName("", "operationResult");
    private final static QName _TestRunMetadata_QNAME = new QName("", "testRunMetadata");
    private final static QName _ProblemOccurrences_QNAME = new QName("", "problemOccurrences");
    private final static QName _Change_QNAME = new QName("", "change");
    private final static QName _RelatedEntity_QNAME = new QName("", "relatedEntity");
    private final static QName _Trigger_QNAME = new QName("", "trigger");
    private final static QName _Agents_QNAME = new QName("", "agents");
    private final static QName _Token_QNAME = new QName("", "token");
    private final static QName _FederationServer_QNAME = new QName("", "federationServer");
    private final static QName _LicenseKey_QNAME = new QName("", "licenseKey");
    private final static QName _MetricTags_QNAME = new QName("", "metricTags");
    private final static QName _Plugin_QNAME = new QName("", "plugin");
    private final static QName _Metric_QNAME = new QName("", "metric");
    private final static QName _SnapshotDependencies_QNAME = new QName("", "snapshot-dependencies");
    private final static QName _AgentPools_QNAME = new QName("", "agentPools");
    private final static QName _VcsRootInstances_QNAME = new QName("", "vcs-root-instances");
    private final static QName _Compatibilities_QNAME = new QName("", "compatibilities");
    private final static QName _Agent_QNAME = new QName("", "agent");
    private final static QName _LicensingData_QNAME = new QName("", "licensingData");
    private final static QName _ProgressInfo_QNAME = new QName("", "progress-info");
    private final static QName _NewProjectDescription_QNAME = new QName("", "newProjectDescription");
    private final static QName _EnabledInfo_QNAME = new QName("", "enabledInfo");
    private final static QName _Project_QNAME = new QName("", "project");
    private final static QName _BuildTriggeringOptions_QNAME = new QName("", "buildTriggeringOptions");
    private final static QName _Features_QNAME = new QName("", "features");
    private final static QName _Links_QNAME = new QName("", "links");
    private final static QName _Tag_QNAME = new QName("", "tag");
    private final static QName _Group_QNAME = new QName("", "group");
    private final static QName _PermissionAssignment_QNAME = new QName("", "permissionAssignment");
    private final static QName _TypedValue_QNAME = new QName("", "typedValue");
    private final static QName _TestOccurrences_QNAME = new QName("", "testOccurrences");
    private final static QName _Test_QNAME = new QName("", "test");
    private final static QName _Datas_QNAME = new QName("", "datas");
    private final static QName _PermissionAssignments_QNAME = new QName("", "permissionAssignments");
    private final static QName _BuildCancelRequest_QNAME = new QName("", "buildCancelRequest");
    private final static QName _ProblemOccurrence_QNAME = new QName("", "problemOccurrence");
    private final static QName _Branches_QNAME = new QName("", "branches");
    private final static QName _Steps_QNAME = new QName("", "steps");
    private final static QName _Users_QNAME = new QName("", "users");
    private final static QName _Entry_QNAME = new QName("", "entry");
    private final static QName _ArtifactDependency_QNAME = new QName("", "artifact-dependency");
    private final static QName _Entries_QNAME = new QName("", "entries");
    private final static QName _Tests_QNAME = new QName("", "tests");
    private final static QName _BuildType_QNAME = new QName("", "buildType");
    private final static QName _Files_QNAME = new QName("", "files");
    private final static QName _CloudImages_QNAME = new QName("", "cloudImages");
    private final static QName _RelatedEntities_QNAME = new QName("", "relatedEntities");
    private final static QName _CloudProfile_QNAME = new QName("", "cloudProfile");
    private final static QName _Compatibility_QNAME = new QName("", "compatibility");
    private final static QName _CloudInstances_QNAME = new QName("", "cloudInstances");
    private final static QName _MetricValues_QNAME = new QName("", "metricValues");
    private final static QName _Problems_QNAME = new QName("", "problems");
    private final static QName _RepositoryState_QNAME = new QName("", "repositoryState");
    private final static QName _Session_QNAME = new QName("", "session");
    private final static QName _VcsStatus_QNAME = new QName("", "vcsStatus");
    private final static QName _AuditAction_QNAME = new QName("", "auditAction");
    private final static QName _Roles_QNAME = new QName("", "roles");
    private final static QName _Changes_QNAME = new QName("", "changes");
    private final static QName _Link_QNAME = new QName("", "link");
    private final static QName _Investigations_QNAME = new QName("", "investigations");
    private final static QName _VcsRootInstance_QNAME = new QName("", "vcs-root-instance");
    private final static QName _BuildChanges_QNAME = new QName("", "buildChanges");
    private final static QName _NewBuildTypeDescription_QNAME = new QName("", "newBuildTypeDescription");
    private final static QName _Servers_QNAME = new QName("", "servers");
    private final static QName _Related_QNAME = new QName("", "related");
    private final static QName _VcsCheckStatus_QNAME = new QName("", "VcsCheckStatus");
    private final static QName _AuditEvents_QNAME = new QName("", "auditEvents");
    private final static QName _Tokens_QNAME = new QName("", "tokens");
    private final static QName _CloudImage_QNAME = new QName("", "cloudImage");
    private final static QName _Requirements_QNAME = new QName("", "requirements");
    private final static QName _IssuesUsages_QNAME = new QName("", "issuesUsages");
    private final static QName _BuildsForCompare_QNAME = new QName("", "buildsForCompare");
    private final static QName _BuildChange_QNAME = new QName("", "buildChange");
    private final static QName _MultipleOperationResult_QNAME = new QName("", "multipleOperationResult");
    private final static QName _CloudInstance_QNAME = new QName("", "cloudInstance");
    private final static QName _Groups_QNAME = new QName("", "groups");
    private final static QName _Permission_QNAME = new QName("", "permission");
    private final static QName _CompatibilityPolicy_QNAME = new QName("", "compatibilityPolicy");
    private final static QName _ProjectFeature_QNAME = new QName("", "projectFeature");
    private final static QName _Environment_QNAME = new QName("", "environment");
    private final static QName _PinInfo_QNAME = new QName("", "pinInfo");
    private final static QName _Build_QNAME = new QName("", "build");
    private final static QName _BuildTypes_QNAME = new QName("", "buildTypes");
    private final static QName _SnapshotDependency_QNAME = new QName("", "snapshot-dependency");
    private final static QName _Investigation_QNAME = new QName("", "investigation");
    private final static QName _Comment_QNAME = new QName("", "comment");
    private final static QName _Step_QNAME = new QName("", "step");
    private final static QName _VcsRoots_QNAME = new QName("", "vcs-roots");
    private final static QName _BooleanStatus_QNAME = new QName("", "booleanStatus");
    private final static QName _User_QNAME = new QName("", "user");
    private final static QName _Properties_QNAME = new QName("", "properties");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.jetbrains.teamcity.rest.client.jaxrs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MetricTag }
     * 
     */
    public MetricTag createMetricTag() {
        return new MetricTag();
    }

    /**
     * Create an instance of {@link Projects }
     * 
     */
    public Projects createProjects() {
        return new Projects();
    }

    /**
     * Create an instance of {@link PluginInfos }
     * 
     */
    public PluginInfos createPluginInfos() {
        return new PluginInfos();
    }

    /**
     * Create an instance of {@link TestOccurrence }
     * 
     */
    public TestOccurrence createTestOccurrence() {
        return new TestOccurrence();
    }

    /**
     * Create an instance of {@link AuditEvent }
     * 
     */
    public AuditEvent createAuditEvent() {
        return new AuditEvent();
    }

    /**
     * Create an instance of {@link ParameterType }
     * 
     */
    public ParameterType createParameterType() {
        return new ParameterType();
    }

    /**
     * Create an instance of {@link Branch }
     * 
     */
    public Branch createBranch() {
        return new Branch();
    }

    /**
     * Create an instance of {@link Issues }
     * 
     */
    public Issues createIssues() {
        return new Issues();
    }

    /**
     * Create an instance of {@link CloudProfiles }
     * 
     */
    public CloudProfiles createCloudProfiles() {
        return new CloudProfiles();
    }

    /**
     * Create an instance of {@link LicenseKeys }
     * 
     */
    public LicenseKeys createLicenseKeys() {
        return new LicenseKeys();
    }

    /**
     * Create an instance of {@link Problem }
     * 
     */
    public Problem createProblem() {
        return new Problem();
    }

    /**
     * Create an instance of {@link Builds }
     * 
     */
    public Builds createBuilds() {
        return new Builds();
    }

    /**
     * Create an instance of {@link Property }
     * 
     */
    public Property createProperty() {
        return new Property();
    }

    /**
     * Create an instance of {@link HReference }
     * 
     */
    public HReference createHReference() {
        return new HReference();
    }

    /**
     * Create an instance of {@link Sessions }
     * 
     */
    public Sessions createSessions() {
        return new Sessions();
    }

    /**
     * Create an instance of {@link PropEntitiesArtifactDep }
     * 
     */
    public PropEntitiesArtifactDep createPropEntitiesArtifactDep() {
        return new PropEntitiesArtifactDep();
    }

    /**
     * Create an instance of {@link VcsRoot }
     * 
     */
    public VcsRoot createVcsRoot() {
        return new VcsRoot();
    }

    /**
     * Create an instance of {@link Mute }
     * 
     */
    public Mute createMute() {
        return new Mute();
    }

    /**
     * Create an instance of {@link PropEntitiesTrigger }
     * 
     */
    public PropEntitiesTrigger createPropEntitiesTrigger() {
        return new PropEntitiesTrigger();
    }

    /**
     * Create an instance of {@link Tags }
     * 
     */
    public Tags createTags() {
        return new Tags();
    }

    /**
     * Create an instance of {@link PropEntitiesAgentRequirement }
     * 
     */
    public PropEntitiesAgentRequirement createPropEntitiesAgentRequirement() {
        return new PropEntitiesAgentRequirement();
    }

    /**
     * Create an instance of {@link VcsRootEntries }
     * 
     */
    public VcsRootEntries createVcsRootEntries() {
        return new VcsRootEntries();
    }

    /**
     * Create an instance of {@link Metrics }
     * 
     */
    public Metrics createMetrics() {
        return new Metrics();
    }

    /**
     * Create an instance of {@link Items }
     * 
     */
    public Items createItems() {
        return new Items();
    }

    /**
     * Create an instance of {@link PropEntitiesProjectFeature }
     * 
     */
    public PropEntitiesProjectFeature createPropEntitiesProjectFeature() {
        return new PropEntitiesProjectFeature();
    }

    /**
     * Create an instance of {@link Server }
     * 
     */
    public Server createServer() {
        return new Server();
    }

    /**
     * Create an instance of {@link RoleAssignment }
     * 
     */
    public RoleAssignment createRoleAssignment() {
        return new RoleAssignment();
    }

    /**
     * Create an instance of {@link VcsRootEntry }
     * 
     */
    public VcsRootEntry createVcsRootEntry() {
        return new VcsRootEntry();
    }

    /**
     * Create an instance of {@link MetricValue }
     * 
     */
    public MetricValue createMetricValue() {
        return new MetricValue();
    }

    /**
     * Create an instance of {@link Mutes }
     * 
     */
    public Mutes createMutes() {
        return new Mutes();
    }

    /**
     * Create an instance of {@link BranchVersion }
     * 
     */
    public BranchVersion createBranchVersion() {
        return new BranchVersion();
    }

    /**
     * Create an instance of {@link NamedData }
     * 
     */
    public NamedData createNamedData() {
        return new NamedData();
    }

    /**
     * Create an instance of {@link File }
     * 
     */
    public File createFile() {
        return new File();
    }

    /**
     * Create an instance of {@link PropEntityFeature }
     * 
     */
    public PropEntityFeature createPropEntityFeature() {
        return new PropEntityFeature();
    }

    /**
     * Create an instance of {@link VcsLabelingOptions }
     * 
     */
    public VcsLabelingOptions createVcsLabelingOptions() {
        return new VcsLabelingOptions();
    }

    /**
     * Create an instance of {@link PropEntityAgentRequirement }
     * 
     */
    public PropEntityAgentRequirement createPropEntityAgentRequirement() {
        return new PropEntityAgentRequirement();
    }

    /**
     * Create an instance of {@link AgentPool }
     * 
     */
    public AgentPool createAgentPool() {
        return new AgentPool();
    }

    /**
     * Create an instance of {@link AuthorizedInfo }
     * 
     */
    public AuthorizedInfo createAuthorizedInfo() {
        return new AuthorizedInfo();
    }

    /**
     * Create an instance of {@link OperationResult }
     * 
     */
    public OperationResult createOperationResult() {
        return new OperationResult();
    }

    /**
     * Create an instance of {@link TestRunMetadata }
     * 
     */
    public TestRunMetadata createTestRunMetadata() {
        return new TestRunMetadata();
    }

    /**
     * Create an instance of {@link ProblemOccurrences }
     * 
     */
    public ProblemOccurrences createProblemOccurrences() {
        return new ProblemOccurrences();
    }

    /**
     * Create an instance of {@link Change }
     * 
     */
    public Change createChange() {
        return new Change();
    }

    /**
     * Create an instance of {@link RelatedEntity }
     * 
     */
    public RelatedEntity createRelatedEntity() {
        return new RelatedEntity();
    }

    /**
     * Create an instance of {@link PropEntityTrigger }
     * 
     */
    public PropEntityTrigger createPropEntityTrigger() {
        return new PropEntityTrigger();
    }

    /**
     * Create an instance of {@link AgentsRef }
     * 
     */
    public AgentsRef createAgentsRef() {
        return new AgentsRef();
    }

    /**
     * Create an instance of {@link Token }
     * 
     */
    public Token createToken() {
        return new Token();
    }

    /**
     * Create an instance of {@link FederationServer }
     * 
     */
    public FederationServer createFederationServer() {
        return new FederationServer();
    }

    /**
     * Create an instance of {@link LicenseKey }
     * 
     */
    public LicenseKey createLicenseKey() {
        return new LicenseKey();
    }

    /**
     * Create an instance of {@link MetricTags }
     * 
     */
    public MetricTags createMetricTags() {
        return new MetricTags();
    }

    /**
     * Create an instance of {@link PluginInfo }
     * 
     */
    public PluginInfo createPluginInfo() {
        return new PluginInfo();
    }

    /**
     * Create an instance of {@link Metric }
     * 
     */
    public Metric createMetric() {
        return new Metric();
    }

    /**
     * Create an instance of {@link PropEntitiesSnapshotDep }
     * 
     */
    public PropEntitiesSnapshotDep createPropEntitiesSnapshotDep() {
        return new PropEntitiesSnapshotDep();
    }

    /**
     * Create an instance of {@link AgentPools }
     * 
     */
    public AgentPools createAgentPools() {
        return new AgentPools();
    }

    /**
     * Create an instance of {@link VcsRootInstances }
     * 
     */
    public VcsRootInstances createVcsRootInstances() {
        return new VcsRootInstances();
    }

    /**
     * Create an instance of {@link Compatibilities }
     * 
     */
    public Compatibilities createCompatibilities() {
        return new Compatibilities();
    }

    /**
     * Create an instance of {@link Agent }
     * 
     */
    public Agent createAgent() {
        return new Agent();
    }

    /**
     * Create an instance of {@link LicensingData }
     * 
     */
    public LicensingData createLicensingData() {
        return new LicensingData();
    }

    /**
     * Create an instance of {@link RunningBuildInfo }
     * 
     */
    public RunningBuildInfo createRunningBuildInfo() {
        return new RunningBuildInfo();
    }

    /**
     * Create an instance of {@link NewProjectDescription }
     * 
     */
    public NewProjectDescription createNewProjectDescription() {
        return new NewProjectDescription();
    }

    /**
     * Create an instance of {@link EnabledInfo }
     * 
     */
    public EnabledInfo createEnabledInfo() {
        return new EnabledInfo();
    }

    /**
     * Create an instance of {@link Project }
     * 
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link BuildTriggeringOptions }
     * 
     */
    public BuildTriggeringOptions createBuildTriggeringOptions() {
        return new BuildTriggeringOptions();
    }

    /**
     * Create an instance of {@link PropEntitiesFeature }
     * 
     */
    public PropEntitiesFeature createPropEntitiesFeature() {
        return new PropEntitiesFeature();
    }

    /**
     * Create an instance of {@link Links }
     * 
     */
    public Links createLinks() {
        return new Links();
    }

    /**
     * Create an instance of {@link Tag }
     * 
     */
    public Tag createTag() {
        return new Tag();
    }

    /**
     * Create an instance of {@link Group }
     * 
     */
    public Group createGroup() {
        return new Group();
    }

    /**
     * Create an instance of {@link PermissionAssignment }
     * 
     */
    public PermissionAssignment createPermissionAssignment() {
        return new PermissionAssignment();
    }

    /**
     * Create an instance of {@link TypedValue }
     * 
     */
    public TypedValue createTypedValue() {
        return new TypedValue();
    }

    /**
     * Create an instance of {@link TestOccurrences }
     * 
     */
    public TestOccurrences createTestOccurrences() {
        return new TestOccurrences();
    }

    /**
     * Create an instance of {@link Test }
     * 
     */
    public Test createTest() {
        return new Test();
    }

    /**
     * Create an instance of {@link NamedDatas }
     * 
     */
    public NamedDatas createNamedDatas() {
        return new NamedDatas();
    }

    /**
     * Create an instance of {@link PermissionAssignments }
     * 
     */
    public PermissionAssignments createPermissionAssignments() {
        return new PermissionAssignments();
    }

    /**
     * Create an instance of {@link BuildCancelRequest }
     * 
     */
    public BuildCancelRequest createBuildCancelRequest() {
        return new BuildCancelRequest();
    }

    /**
     * Create an instance of {@link ProblemOccurrence }
     * 
     */
    public ProblemOccurrence createProblemOccurrence() {
        return new ProblemOccurrence();
    }

    /**
     * Create an instance of {@link Branches }
     * 
     */
    public Branches createBranches() {
        return new Branches();
    }

    /**
     * Create an instance of {@link PropEntitiesStep }
     * 
     */
    public PropEntitiesStep createPropEntitiesStep() {
        return new PropEntitiesStep();
    }

    /**
     * Create an instance of {@link Users }
     * 
     */
    public Users createUsers() {
        return new Users();
    }

    /**
     * Create an instance of {@link Entry }
     * 
     */
    public Entry createEntry() {
        return new Entry();
    }

    /**
     * Create an instance of {@link PropEntityArtifactDep }
     * 
     */
    public PropEntityArtifactDep createPropEntityArtifactDep() {
        return new PropEntityArtifactDep();
    }

    /**
     * Create an instance of {@link Entries }
     * 
     */
    public Entries createEntries() {
        return new Entries();
    }

    /**
     * Create an instance of {@link Tests }
     * 
     */
    public Tests createTests() {
        return new Tests();
    }

    /**
     * Create an instance of {@link BuildType }
     * 
     */
    public BuildType createBuildType() {
        return new BuildType();
    }

    /**
     * Create an instance of {@link Files }
     * 
     */
    public Files createFiles() {
        return new Files();
    }

    /**
     * Create an instance of {@link CloudImages }
     * 
     */
    public CloudImages createCloudImages() {
        return new CloudImages();
    }

    /**
     * Create an instance of {@link RelatedEntities }
     * 
     */
    public RelatedEntities createRelatedEntities() {
        return new RelatedEntities();
    }

    /**
     * Create an instance of {@link CloudProfile }
     * 
     */
    public CloudProfile createCloudProfile() {
        return new CloudProfile();
    }

    /**
     * Create an instance of {@link Compatibility }
     * 
     */
    public Compatibility createCompatibility() {
        return new Compatibility();
    }

    /**
     * Create an instance of {@link CloudInstances }
     * 
     */
    public CloudInstances createCloudInstances() {
        return new CloudInstances();
    }

    /**
     * Create an instance of {@link MetricValues }
     * 
     */
    public MetricValues createMetricValues() {
        return new MetricValues();
    }

    /**
     * Create an instance of {@link Problems }
     * 
     */
    public Problems createProblems() {
        return new Problems();
    }

    /**
     * Create an instance of {@link RepositoryState }
     * 
     */
    public RepositoryState createRepositoryState() {
        return new RepositoryState();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link VcsStatus }
     * 
     */
    public VcsStatus createVcsStatus() {
        return new VcsStatus();
    }

    /**
     * Create an instance of {@link AuditAction }
     * 
     */
    public AuditAction createAuditAction() {
        return new AuditAction();
    }

    /**
     * Create an instance of {@link RoleAssignments }
     * 
     */
    public RoleAssignments createRoleAssignments() {
        return new RoleAssignments();
    }

    /**
     * Create an instance of {@link Changes }
     * 
     */
    public Changes createChanges() {
        return new Changes();
    }

    /**
     * Create an instance of {@link Link }
     * 
     */
    public Link createLink() {
        return new Link();
    }

    /**
     * Create an instance of {@link Investigations }
     * 
     */
    public Investigations createInvestigations() {
        return new Investigations();
    }

    /**
     * Create an instance of {@link VcsRootInstance }
     * 
     */
    public VcsRootInstance createVcsRootInstance() {
        return new VcsRootInstance();
    }

    /**
     * Create an instance of {@link BuildChanges }
     * 
     */
    public BuildChanges createBuildChanges() {
        return new BuildChanges();
    }

    /**
     * Create an instance of {@link NewBuildTypeDescription }
     * 
     */
    public NewBuildTypeDescription createNewBuildTypeDescription() {
        return new NewBuildTypeDescription();
    }

    /**
     * Create an instance of {@link FederationServers }
     * 
     */
    public FederationServers createFederationServers() {
        return new FederationServers();
    }

    /**
     * Create an instance of {@link Related }
     * 
     */
    public Related createRelated() {
        return new Related();
    }

    /**
     * Create an instance of {@link VcsCheckStatus }
     * 
     */
    public VcsCheckStatus createVcsCheckStatus() {
        return new VcsCheckStatus();
    }

    /**
     * Create an instance of {@link AuditEvents }
     * 
     */
    public AuditEvents createAuditEvents() {
        return new AuditEvents();
    }

    /**
     * Create an instance of {@link Tokens }
     * 
     */
    public Tokens createTokens() {
        return new Tokens();
    }

    /**
     * Create an instance of {@link CloudImage }
     * 
     */
    public CloudImage createCloudImage() {
        return new CloudImage();
    }

    /**
     * Create an instance of {@link Requirements }
     * 
     */
    public Requirements createRequirements() {
        return new Requirements();
    }

    /**
     * Create an instance of {@link IssueUsages }
     * 
     */
    public IssueUsages createIssueUsages() {
        return new IssueUsages();
    }

    /**
     * Create an instance of {@link BuildsForCompare }
     * 
     */
    public BuildsForCompare createBuildsForCompare() {
        return new BuildsForCompare();
    }

    /**
     * Create an instance of {@link BuildChange }
     * 
     */
    public BuildChange createBuildChange() {
        return new BuildChange();
    }

    /**
     * Create an instance of {@link MultipleOperationResult }
     * 
     */
    public MultipleOperationResult createMultipleOperationResult() {
        return new MultipleOperationResult();
    }

    /**
     * Create an instance of {@link CloudInstance }
     * 
     */
    public CloudInstance createCloudInstance() {
        return new CloudInstance();
    }

    /**
     * Create an instance of {@link Groups }
     * 
     */
    public Groups createGroups() {
        return new Groups();
    }

    /**
     * Create an instance of {@link Permission }
     * 
     */
    public Permission createPermission() {
        return new Permission();
    }

    /**
     * Create an instance of {@link CompatibilityPolicy }
     * 
     */
    public CompatibilityPolicy createCompatibilityPolicy() {
        return new CompatibilityPolicy();
    }

    /**
     * Create an instance of {@link PropEntityProjectFeature }
     * 
     */
    public PropEntityProjectFeature createPropEntityProjectFeature() {
        return new PropEntityProjectFeature();
    }

    /**
     * Create an instance of {@link Environment }
     * 
     */
    public Environment createEnvironment() {
        return new Environment();
    }

    /**
     * Create an instance of {@link PinInfo }
     * 
     */
    public PinInfo createPinInfo() {
        return new PinInfo();
    }

    /**
     * Create an instance of {@link Build }
     * 
     */
    public Build createBuild() {
        return new Build();
    }

    /**
     * Create an instance of {@link BuildTypes }
     * 
     */
    public BuildTypes createBuildTypes() {
        return new BuildTypes();
    }

    /**
     * Create an instance of {@link PropEntitySnapshotDep }
     * 
     */
    public PropEntitySnapshotDep createPropEntitySnapshotDep() {
        return new PropEntitySnapshotDep();
    }

    /**
     * Create an instance of {@link Investigation }
     * 
     */
    public Investigation createInvestigation() {
        return new Investigation();
    }

    /**
     * Create an instance of {@link Comment }
     * 
     */
    public Comment createComment() {
        return new Comment();
    }

    /**
     * Create an instance of {@link PropEntityStep }
     * 
     */
    public PropEntityStep createPropEntityStep() {
        return new PropEntityStep();
    }

    /**
     * Create an instance of {@link VcsRoots }
     * 
     */
    public VcsRoots createVcsRoots() {
        return new VcsRoots();
    }

    /**
     * Create an instance of {@link BooleanStatus }
     * 
     */
    public BooleanStatus createBooleanStatus() {
        return new BooleanStatus();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link Properties }
     * 
     */
    public Properties createProperties() {
        return new Properties();
    }

    /**
     * Create an instance of {@link Resolution }
     * 
     */
    public Resolution createResolution() {
        return new Resolution();
    }

    /**
     * Create an instance of {@link Issue }
     * 
     */
    public Issue createIssue() {
        return new Issue();
    }

    /**
     * Create an instance of {@link FileChange }
     * 
     */
    public FileChange createFileChange() {
        return new FileChange();
    }

    /**
     * Create an instance of {@link IssueUsage }
     * 
     */
    public IssueUsage createIssueUsage() {
        return new IssueUsage();
    }

    /**
     * Create an instance of {@link ProblemTarget }
     * 
     */
    public ProblemTarget createProblemTarget() {
        return new ProblemTarget();
    }

    /**
     * Create an instance of {@link CopyOptionsDescription }
     * 
     */
    public CopyOptionsDescription createCopyOptionsDescription() {
        return new CopyOptionsDescription();
    }

    /**
     * Create an instance of {@link TriggeredBy }
     * 
     */
    public TriggeredBy createTriggeredBy() {
        return new TriggeredBy();
    }

    /**
     * Create an instance of {@link FileChanges }
     * 
     */
    public FileChanges createFileChanges() {
        return new FileChanges();
    }

    /**
     * Create an instance of {@link OccurrencesSummary }
     * 
     */
    public OccurrencesSummary createOccurrencesSummary() {
        return new OccurrencesSummary();
    }

    /**
     * Create an instance of {@link Revision }
     * 
     */
    public Revision createRevision() {
        return new Revision();
    }

    /**
     * Create an instance of {@link Revisions }
     * 
     */
    public Revisions createRevisions() {
        return new Revisions();
    }

    /**
     * Create an instance of {@link ProblemScope }
     * 
     */
    public ProblemScope createProblemScope() {
        return new ProblemScope();
    }

    /**
     * Create an instance of {@link PropEntity }
     * 
     */
    public PropEntity createPropEntity() {
        return new PropEntity();
    }

    /**
     * Create an instance of {@link StateField }
     * 
     */
    public StateField createStateField() {
        return new StateField();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetricTag }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metricTag")
    public JAXBElement<MetricTag> createMetricTag(MetricTag value) {
        return new JAXBElement<MetricTag>(_MetricTag_QNAME, MetricTag.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Projects }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "projects")
    public JAXBElement<Projects> createProjects(Projects value) {
        return new JAXBElement<Projects>(_Projects_QNAME, Projects.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PluginInfos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "plugins")
    public JAXBElement<PluginInfos> createPlugins(PluginInfos value) {
        return new JAXBElement<PluginInfos>(_Plugins_QNAME, PluginInfos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestOccurrence }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "testOccurrence")
    public JAXBElement<TestOccurrence> createTestOccurrence(TestOccurrence value) {
        return new JAXBElement<TestOccurrence>(_TestOccurrence_QNAME, TestOccurrence.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuditEvent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "auditEvent")
    public JAXBElement<AuditEvent> createAuditEvent(AuditEvent value) {
        return new JAXBElement<AuditEvent>(_AuditEvent_QNAME, AuditEvent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParameterType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "type")
    public JAXBElement<ParameterType> createType(ParameterType value) {
        return new JAXBElement<ParameterType>(_Type_QNAME, ParameterType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Branch }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "branch")
    public JAXBElement<Branch> createBranch(Branch value) {
        return new JAXBElement<Branch>(_Branch_QNAME, Branch.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Issues }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "issues")
    public JAXBElement<Issues> createIssues(Issues value) {
        return new JAXBElement<Issues>(_Issues_QNAME, Issues.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloudProfiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cloudProfiles")
    public JAXBElement<CloudProfiles> createCloudProfiles(CloudProfiles value) {
        return new JAXBElement<CloudProfiles>(_CloudProfiles_QNAME, CloudProfiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LicenseKeys }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "licenseKeys")
    public JAXBElement<LicenseKeys> createLicenseKeys(LicenseKeys value) {
        return new JAXBElement<LicenseKeys>(_LicenseKeys_QNAME, LicenseKeys.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Problem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "problem")
    public JAXBElement<Problem> createProblem(Problem value) {
        return new JAXBElement<Problem>(_Problem_QNAME, Problem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Builds }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "builds")
    public JAXBElement<Builds> createBuilds(Builds value) {
        return new JAXBElement<Builds>(_Builds_QNAME, Builds.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Property }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "property")
    public JAXBElement<Property> createProperty(Property value) {
        return new JAXBElement<Property>(_Property_QNAME, Property.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HReference }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "href")
    public JAXBElement<HReference> createHref(HReference value) {
        return new JAXBElement<HReference>(_Href_QNAME, HReference.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Sessions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sessions")
    public JAXBElement<Sessions> createSessions(Sessions value) {
        return new JAXBElement<Sessions>(_Sessions_QNAME, Sessions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntitiesArtifactDep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "artifact-dependencies")
    public JAXBElement<PropEntitiesArtifactDep> createArtifactDependencies(PropEntitiesArtifactDep value) {
        return new JAXBElement<PropEntitiesArtifactDep>(_ArtifactDependencies_QNAME, PropEntitiesArtifactDep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VcsRoot }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vcs-root")
    public JAXBElement<VcsRoot> createVcsRoot(VcsRoot value) {
        return new JAXBElement<VcsRoot>(_VcsRoot_QNAME, VcsRoot.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Mute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mute")
    public JAXBElement<Mute> createMute(Mute value) {
        return new JAXBElement<Mute>(_Mute_QNAME, Mute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntitiesTrigger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "triggers")
    public JAXBElement<PropEntitiesTrigger> createTriggers(PropEntitiesTrigger value) {
        return new JAXBElement<PropEntitiesTrigger>(_Triggers_QNAME, PropEntitiesTrigger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tags }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tags")
    public JAXBElement<Tags> createTags(Tags value) {
        return new JAXBElement<Tags>(_Tags_QNAME, Tags.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntitiesAgentRequirement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agent-requirements")
    public JAXBElement<PropEntitiesAgentRequirement> createAgentRequirements(PropEntitiesAgentRequirement value) {
        return new JAXBElement<PropEntitiesAgentRequirement>(_AgentRequirements_QNAME, PropEntitiesAgentRequirement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VcsRootEntries }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vcs-root-entries")
    public JAXBElement<VcsRootEntries> createVcsRootEntries(VcsRootEntries value) {
        return new JAXBElement<VcsRootEntries>(_VcsRootEntries_QNAME, VcsRootEntries.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Metrics }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metrics")
    public JAXBElement<Metrics> createMetrics(Metrics value) {
        return new JAXBElement<Metrics>(_Metrics_QNAME, Metrics.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Items }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "items")
    public JAXBElement<Items> createItems(Items value) {
        return new JAXBElement<Items>(_Items_QNAME, Items.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntitiesProjectFeature }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "projectFeatures")
    public JAXBElement<PropEntitiesProjectFeature> createProjectFeatures(PropEntitiesProjectFeature value) {
        return new JAXBElement<PropEntitiesProjectFeature>(_ProjectFeatures_QNAME, PropEntitiesProjectFeature.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Server }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "server")
    public JAXBElement<Server> createServer(Server value) {
        return new JAXBElement<Server>(_Server_QNAME, Server.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RoleAssignment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "role")
    public JAXBElement<RoleAssignment> createRole(RoleAssignment value) {
        return new JAXBElement<RoleAssignment>(_Role_QNAME, RoleAssignment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VcsRootEntry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vcs-root-entry")
    public JAXBElement<VcsRootEntry> createVcsRootEntry(VcsRootEntry value) {
        return new JAXBElement<VcsRootEntry>(_VcsRootEntry_QNAME, VcsRootEntry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetricValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metricValue")
    public JAXBElement<MetricValue> createMetricValue(MetricValue value) {
        return new JAXBElement<MetricValue>(_MetricValue_QNAME, MetricValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Mutes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mutes")
    public JAXBElement<Mutes> createMutes(Mutes value) {
        return new JAXBElement<Mutes>(_Mutes_QNAME, Mutes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BranchVersion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "branchVersion")
    public JAXBElement<BranchVersion> createBranchVersion(BranchVersion value) {
        return new JAXBElement<BranchVersion>(_BranchVersion_QNAME, BranchVersion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NamedData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metaData")
    public JAXBElement<NamedData> createMetaData(NamedData value) {
        return new JAXBElement<NamedData>(_MetaData_QNAME, NamedData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link File }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "file")
    public JAXBElement<File> createFile(File value) {
        return new JAXBElement<File>(_File_QNAME, File.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntityFeature }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "feature")
    public JAXBElement<PropEntityFeature> createFeature(PropEntityFeature value) {
        return new JAXBElement<PropEntityFeature>(_Feature_QNAME, PropEntityFeature.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VcsLabelingOptions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vcsLabeling")
    public JAXBElement<VcsLabelingOptions> createVcsLabeling(VcsLabelingOptions value) {
        return new JAXBElement<VcsLabelingOptions>(_VcsLabeling_QNAME, VcsLabelingOptions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntityAgentRequirement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agent-requirement")
    public JAXBElement<PropEntityAgentRequirement> createAgentRequirement(PropEntityAgentRequirement value) {
        return new JAXBElement<PropEntityAgentRequirement>(_AgentRequirement_QNAME, PropEntityAgentRequirement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgentPool }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agentPool")
    public JAXBElement<AgentPool> createAgentPool(AgentPool value) {
        return new JAXBElement<AgentPool>(_AgentPool_QNAME, AgentPool.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizedInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "authorizedInfo")
    public JAXBElement<AuthorizedInfo> createAuthorizedInfo(AuthorizedInfo value) {
        return new JAXBElement<AuthorizedInfo>(_AuthorizedInfo_QNAME, AuthorizedInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OperationResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "operationResult")
    public JAXBElement<OperationResult> createOperationResult(OperationResult value) {
        return new JAXBElement<OperationResult>(_OperationResult_QNAME, OperationResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestRunMetadata }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "testRunMetadata")
    public JAXBElement<TestRunMetadata> createTestRunMetadata(TestRunMetadata value) {
        return new JAXBElement<TestRunMetadata>(_TestRunMetadata_QNAME, TestRunMetadata.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProblemOccurrences }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "problemOccurrences")
    public JAXBElement<ProblemOccurrences> createProblemOccurrences(ProblemOccurrences value) {
        return new JAXBElement<ProblemOccurrences>(_ProblemOccurrences_QNAME, ProblemOccurrences.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Change }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "change")
    public JAXBElement<Change> createChange(Change value) {
        return new JAXBElement<Change>(_Change_QNAME, Change.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RelatedEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "relatedEntity")
    public JAXBElement<RelatedEntity> createRelatedEntity(RelatedEntity value) {
        return new JAXBElement<RelatedEntity>(_RelatedEntity_QNAME, RelatedEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntityTrigger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "trigger")
    public JAXBElement<PropEntityTrigger> createTrigger(PropEntityTrigger value) {
        return new JAXBElement<PropEntityTrigger>(_Trigger_QNAME, PropEntityTrigger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgentsRef }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agents")
    public JAXBElement<AgentsRef> createAgents(AgentsRef value) {
        return new JAXBElement<AgentsRef>(_Agents_QNAME, AgentsRef.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Token }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "token")
    public JAXBElement<Token> createToken(Token value) {
        return new JAXBElement<Token>(_Token_QNAME, Token.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FederationServer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "federationServer")
    public JAXBElement<FederationServer> createFederationServer(FederationServer value) {
        return new JAXBElement<FederationServer>(_FederationServer_QNAME, FederationServer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LicenseKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "licenseKey")
    public JAXBElement<LicenseKey> createLicenseKey(LicenseKey value) {
        return new JAXBElement<LicenseKey>(_LicenseKey_QNAME, LicenseKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetricTags }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metricTags")
    public JAXBElement<MetricTags> createMetricTags(MetricTags value) {
        return new JAXBElement<MetricTags>(_MetricTags_QNAME, MetricTags.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PluginInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "plugin")
    public JAXBElement<PluginInfo> createPlugin(PluginInfo value) {
        return new JAXBElement<PluginInfo>(_Plugin_QNAME, PluginInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Metric }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metric")
    public JAXBElement<Metric> createMetric(Metric value) {
        return new JAXBElement<Metric>(_Metric_QNAME, Metric.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntitiesSnapshotDep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "snapshot-dependencies")
    public JAXBElement<PropEntitiesSnapshotDep> createSnapshotDependencies(PropEntitiesSnapshotDep value) {
        return new JAXBElement<PropEntitiesSnapshotDep>(_SnapshotDependencies_QNAME, PropEntitiesSnapshotDep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgentPools }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agentPools")
    public JAXBElement<AgentPools> createAgentPools(AgentPools value) {
        return new JAXBElement<AgentPools>(_AgentPools_QNAME, AgentPools.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VcsRootInstances }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vcs-root-instances")
    public JAXBElement<VcsRootInstances> createVcsRootInstances(VcsRootInstances value) {
        return new JAXBElement<VcsRootInstances>(_VcsRootInstances_QNAME, VcsRootInstances.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Compatibilities }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "compatibilities")
    public JAXBElement<Compatibilities> createCompatibilities(Compatibilities value) {
        return new JAXBElement<Compatibilities>(_Compatibilities_QNAME, Compatibilities.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Agent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agent")
    public JAXBElement<Agent> createAgent(Agent value) {
        return new JAXBElement<Agent>(_Agent_QNAME, Agent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LicensingData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "licensingData")
    public JAXBElement<LicensingData> createLicensingData(LicensingData value) {
        return new JAXBElement<LicensingData>(_LicensingData_QNAME, LicensingData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RunningBuildInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "progress-info")
    public JAXBElement<RunningBuildInfo> createProgressInfo(RunningBuildInfo value) {
        return new JAXBElement<RunningBuildInfo>(_ProgressInfo_QNAME, RunningBuildInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewProjectDescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "newProjectDescription")
    public JAXBElement<NewProjectDescription> createNewProjectDescription(NewProjectDescription value) {
        return new JAXBElement<NewProjectDescription>(_NewProjectDescription_QNAME, NewProjectDescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnabledInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "enabledInfo")
    public JAXBElement<EnabledInfo> createEnabledInfo(EnabledInfo value) {
        return new JAXBElement<EnabledInfo>(_EnabledInfo_QNAME, EnabledInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Project }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "project")
    public JAXBElement<Project> createProject(Project value) {
        return new JAXBElement<Project>(_Project_QNAME, Project.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuildTriggeringOptions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "buildTriggeringOptions")
    public JAXBElement<BuildTriggeringOptions> createBuildTriggeringOptions(BuildTriggeringOptions value) {
        return new JAXBElement<BuildTriggeringOptions>(_BuildTriggeringOptions_QNAME, BuildTriggeringOptions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntitiesFeature }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "features")
    public JAXBElement<PropEntitiesFeature> createFeatures(PropEntitiesFeature value) {
        return new JAXBElement<PropEntitiesFeature>(_Features_QNAME, PropEntitiesFeature.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Links }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "links")
    public JAXBElement<Links> createLinks(Links value) {
        return new JAXBElement<Links>(_Links_QNAME, Links.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tag }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tag")
    public JAXBElement<Tag> createTag(Tag value) {
        return new JAXBElement<Tag>(_Tag_QNAME, Tag.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Group }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "group")
    public JAXBElement<Group> createGroup(Group value) {
        return new JAXBElement<Group>(_Group_QNAME, Group.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PermissionAssignment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "permissionAssignment")
    public JAXBElement<PermissionAssignment> createPermissionAssignment(PermissionAssignment value) {
        return new JAXBElement<PermissionAssignment>(_PermissionAssignment_QNAME, PermissionAssignment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypedValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "typedValue")
    public JAXBElement<TypedValue> createTypedValue(TypedValue value) {
        return new JAXBElement<TypedValue>(_TypedValue_QNAME, TypedValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestOccurrences }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "testOccurrences")
    public JAXBElement<TestOccurrences> createTestOccurrences(TestOccurrences value) {
        return new JAXBElement<TestOccurrences>(_TestOccurrences_QNAME, TestOccurrences.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Test }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "test")
    public JAXBElement<Test> createTest(Test value) {
        return new JAXBElement<Test>(_Test_QNAME, Test.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NamedDatas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datas")
    public JAXBElement<NamedDatas> createDatas(NamedDatas value) {
        return new JAXBElement<NamedDatas>(_Datas_QNAME, NamedDatas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PermissionAssignments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "permissionAssignments")
    public JAXBElement<PermissionAssignments> createPermissionAssignments(PermissionAssignments value) {
        return new JAXBElement<PermissionAssignments>(_PermissionAssignments_QNAME, PermissionAssignments.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuildCancelRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "buildCancelRequest")
    public JAXBElement<BuildCancelRequest> createBuildCancelRequest(BuildCancelRequest value) {
        return new JAXBElement<BuildCancelRequest>(_BuildCancelRequest_QNAME, BuildCancelRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProblemOccurrence }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "problemOccurrence")
    public JAXBElement<ProblemOccurrence> createProblemOccurrence(ProblemOccurrence value) {
        return new JAXBElement<ProblemOccurrence>(_ProblemOccurrence_QNAME, ProblemOccurrence.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Branches }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "branches")
    public JAXBElement<Branches> createBranches(Branches value) {
        return new JAXBElement<Branches>(_Branches_QNAME, Branches.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntitiesStep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "steps")
    public JAXBElement<PropEntitiesStep> createSteps(PropEntitiesStep value) {
        return new JAXBElement<PropEntitiesStep>(_Steps_QNAME, PropEntitiesStep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Users }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "users")
    public JAXBElement<Users> createUsers(Users value) {
        return new JAXBElement<Users>(_Users_QNAME, Users.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Entry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "entry")
    public JAXBElement<Entry> createEntry(Entry value) {
        return new JAXBElement<Entry>(_Entry_QNAME, Entry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntityArtifactDep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "artifact-dependency")
    public JAXBElement<PropEntityArtifactDep> createArtifactDependency(PropEntityArtifactDep value) {
        return new JAXBElement<PropEntityArtifactDep>(_ArtifactDependency_QNAME, PropEntityArtifactDep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Entries }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "entries")
    public JAXBElement<Entries> createEntries(Entries value) {
        return new JAXBElement<Entries>(_Entries_QNAME, Entries.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tests }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tests")
    public JAXBElement<Tests> createTests(Tests value) {
        return new JAXBElement<Tests>(_Tests_QNAME, Tests.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuildType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "buildType")
    public JAXBElement<BuildType> createBuildType(BuildType value) {
        return new JAXBElement<BuildType>(_BuildType_QNAME, BuildType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Files }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "files")
    public JAXBElement<Files> createFiles(Files value) {
        return new JAXBElement<Files>(_Files_QNAME, Files.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloudImages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cloudImages")
    public JAXBElement<CloudImages> createCloudImages(CloudImages value) {
        return new JAXBElement<CloudImages>(_CloudImages_QNAME, CloudImages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RelatedEntities }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "relatedEntities")
    public JAXBElement<RelatedEntities> createRelatedEntities(RelatedEntities value) {
        return new JAXBElement<RelatedEntities>(_RelatedEntities_QNAME, RelatedEntities.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloudProfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cloudProfile")
    public JAXBElement<CloudProfile> createCloudProfile(CloudProfile value) {
        return new JAXBElement<CloudProfile>(_CloudProfile_QNAME, CloudProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Compatibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "compatibility")
    public JAXBElement<Compatibility> createCompatibility(Compatibility value) {
        return new JAXBElement<Compatibility>(_Compatibility_QNAME, Compatibility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloudInstances }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cloudInstances")
    public JAXBElement<CloudInstances> createCloudInstances(CloudInstances value) {
        return new JAXBElement<CloudInstances>(_CloudInstances_QNAME, CloudInstances.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetricValues }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metricValues")
    public JAXBElement<MetricValues> createMetricValues(MetricValues value) {
        return new JAXBElement<MetricValues>(_MetricValues_QNAME, MetricValues.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Problems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "problems")
    public JAXBElement<Problems> createProblems(Problems value) {
        return new JAXBElement<Problems>(_Problems_QNAME, Problems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepositoryState }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "repositoryState")
    public JAXBElement<RepositoryState> createRepositoryState(RepositoryState value) {
        return new JAXBElement<RepositoryState>(_RepositoryState_QNAME, RepositoryState.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Session }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "session")
    public JAXBElement<Session> createSession(Session value) {
        return new JAXBElement<Session>(_Session_QNAME, Session.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VcsStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vcsStatus")
    public JAXBElement<VcsStatus> createVcsStatus(VcsStatus value) {
        return new JAXBElement<VcsStatus>(_VcsStatus_QNAME, VcsStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuditAction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "auditAction")
    public JAXBElement<AuditAction> createAuditAction(AuditAction value) {
        return new JAXBElement<AuditAction>(_AuditAction_QNAME, AuditAction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RoleAssignments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "roles")
    public JAXBElement<RoleAssignments> createRoles(RoleAssignments value) {
        return new JAXBElement<RoleAssignments>(_Roles_QNAME, RoleAssignments.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Changes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "changes")
    public JAXBElement<Changes> createChanges(Changes value) {
        return new JAXBElement<Changes>(_Changes_QNAME, Changes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Link }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "link")
    public JAXBElement<Link> createLink(Link value) {
        return new JAXBElement<Link>(_Link_QNAME, Link.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Investigations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "investigations")
    public JAXBElement<Investigations> createInvestigations(Investigations value) {
        return new JAXBElement<Investigations>(_Investigations_QNAME, Investigations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VcsRootInstance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vcs-root-instance")
    public JAXBElement<VcsRootInstance> createVcsRootInstance(VcsRootInstance value) {
        return new JAXBElement<VcsRootInstance>(_VcsRootInstance_QNAME, VcsRootInstance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuildChanges }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "buildChanges")
    public JAXBElement<BuildChanges> createBuildChanges(BuildChanges value) {
        return new JAXBElement<BuildChanges>(_BuildChanges_QNAME, BuildChanges.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewBuildTypeDescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "newBuildTypeDescription")
    public JAXBElement<NewBuildTypeDescription> createNewBuildTypeDescription(NewBuildTypeDescription value) {
        return new JAXBElement<NewBuildTypeDescription>(_NewBuildTypeDescription_QNAME, NewBuildTypeDescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FederationServers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "servers")
    public JAXBElement<FederationServers> createServers(FederationServers value) {
        return new JAXBElement<FederationServers>(_Servers_QNAME, FederationServers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Related }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "related")
    public JAXBElement<Related> createRelated(Related value) {
        return new JAXBElement<Related>(_Related_QNAME, Related.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VcsCheckStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VcsCheckStatus")
    public JAXBElement<VcsCheckStatus> createVcsCheckStatus(VcsCheckStatus value) {
        return new JAXBElement<VcsCheckStatus>(_VcsCheckStatus_QNAME, VcsCheckStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuditEvents }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "auditEvents")
    public JAXBElement<AuditEvents> createAuditEvents(AuditEvents value) {
        return new JAXBElement<AuditEvents>(_AuditEvents_QNAME, AuditEvents.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tokens }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tokens")
    public JAXBElement<Tokens> createTokens(Tokens value) {
        return new JAXBElement<Tokens>(_Tokens_QNAME, Tokens.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloudImage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cloudImage")
    public JAXBElement<CloudImage> createCloudImage(CloudImage value) {
        return new JAXBElement<CloudImage>(_CloudImage_QNAME, CloudImage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Requirements }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "requirements")
    public JAXBElement<Requirements> createRequirements(Requirements value) {
        return new JAXBElement<Requirements>(_Requirements_QNAME, Requirements.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IssueUsages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "issuesUsages")
    public JAXBElement<IssueUsages> createIssuesUsages(IssueUsages value) {
        return new JAXBElement<IssueUsages>(_IssuesUsages_QNAME, IssueUsages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuildsForCompare }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "buildsForCompare")
    public JAXBElement<BuildsForCompare> createBuildsForCompare(BuildsForCompare value) {
        return new JAXBElement<BuildsForCompare>(_BuildsForCompare_QNAME, BuildsForCompare.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuildChange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "buildChange")
    public JAXBElement<BuildChange> createBuildChange(BuildChange value) {
        return new JAXBElement<BuildChange>(_BuildChange_QNAME, BuildChange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MultipleOperationResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "multipleOperationResult")
    public JAXBElement<MultipleOperationResult> createMultipleOperationResult(MultipleOperationResult value) {
        return new JAXBElement<MultipleOperationResult>(_MultipleOperationResult_QNAME, MultipleOperationResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloudInstance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cloudInstance")
    public JAXBElement<CloudInstance> createCloudInstance(CloudInstance value) {
        return new JAXBElement<CloudInstance>(_CloudInstance_QNAME, CloudInstance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Groups }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "groups")
    public JAXBElement<Groups> createGroups(Groups value) {
        return new JAXBElement<Groups>(_Groups_QNAME, Groups.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Permission }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "permission")
    public JAXBElement<Permission> createPermission(Permission value) {
        return new JAXBElement<Permission>(_Permission_QNAME, Permission.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompatibilityPolicy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "compatibilityPolicy")
    public JAXBElement<CompatibilityPolicy> createCompatibilityPolicy(CompatibilityPolicy value) {
        return new JAXBElement<CompatibilityPolicy>(_CompatibilityPolicy_QNAME, CompatibilityPolicy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntityProjectFeature }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "projectFeature")
    public JAXBElement<PropEntityProjectFeature> createProjectFeature(PropEntityProjectFeature value) {
        return new JAXBElement<PropEntityProjectFeature>(_ProjectFeature_QNAME, PropEntityProjectFeature.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Environment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "environment")
    public JAXBElement<Environment> createEnvironment(Environment value) {
        return new JAXBElement<Environment>(_Environment_QNAME, Environment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PinInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pinInfo")
    public JAXBElement<PinInfo> createPinInfo(PinInfo value) {
        return new JAXBElement<PinInfo>(_PinInfo_QNAME, PinInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Build }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "build")
    public JAXBElement<Build> createBuild(Build value) {
        return new JAXBElement<Build>(_Build_QNAME, Build.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuildTypes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "buildTypes")
    public JAXBElement<BuildTypes> createBuildTypes(BuildTypes value) {
        return new JAXBElement<BuildTypes>(_BuildTypes_QNAME, BuildTypes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntitySnapshotDep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "snapshot-dependency")
    public JAXBElement<PropEntitySnapshotDep> createSnapshotDependency(PropEntitySnapshotDep value) {
        return new JAXBElement<PropEntitySnapshotDep>(_SnapshotDependency_QNAME, PropEntitySnapshotDep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Investigation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "investigation")
    public JAXBElement<Investigation> createInvestigation(Investigation value) {
        return new JAXBElement<Investigation>(_Investigation_QNAME, Investigation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Comment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "comment")
    public JAXBElement<Comment> createComment(Comment value) {
        return new JAXBElement<Comment>(_Comment_QNAME, Comment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropEntityStep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "step")
    public JAXBElement<PropEntityStep> createStep(PropEntityStep value) {
        return new JAXBElement<PropEntityStep>(_Step_QNAME, PropEntityStep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VcsRoots }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vcs-roots")
    public JAXBElement<VcsRoots> createVcsRoots(VcsRoots value) {
        return new JAXBElement<VcsRoots>(_VcsRoots_QNAME, VcsRoots.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BooleanStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "booleanStatus")
    public JAXBElement<BooleanStatus> createBooleanStatus(BooleanStatus value) {
        return new JAXBElement<BooleanStatus>(_BooleanStatus_QNAME, BooleanStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "user")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Properties }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "properties")
    public JAXBElement<Properties> createProperties(Properties value) {
        return new JAXBElement<Properties>(_Properties_QNAME, Properties.class, null, value);
    }

}
