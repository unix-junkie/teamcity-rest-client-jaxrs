
package org.jetbrains.teamcity.rest.client.jaxrs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for buildsForCompare complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="buildsForCompare"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}builds"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lastPinned" type="{}build" minOccurs="0"/&gt;
 *         &lt;element name="lastSuccessful" type="{}build" minOccurs="0"/&gt;
 *         &lt;element name="lastFinished" type="{}build" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "buildsForCompare", propOrder = {
    "lastPinned",
    "lastSuccessful",
    "lastFinished"
})
public class BuildsForCompare
    extends Builds
{

    protected Build lastPinned;
    protected Build lastSuccessful;
    protected Build lastFinished;

    /**
     * Gets the value of the lastPinned property.
     * 
     * @return
     *     possible object is
     *     {@link Build }
     *     
     */
    public Build getLastPinned() {
        return lastPinned;
    }

    /**
     * Sets the value of the lastPinned property.
     * 
     * @param value
     *     allowed object is
     *     {@link Build }
     *     
     */
    public void setLastPinned(Build value) {
        this.lastPinned = value;
    }

    /**
     * Gets the value of the lastSuccessful property.
     * 
     * @return
     *     possible object is
     *     {@link Build }
     *     
     */
    public Build getLastSuccessful() {
        return lastSuccessful;
    }

    /**
     * Sets the value of the lastSuccessful property.
     * 
     * @param value
     *     allowed object is
     *     {@link Build }
     *     
     */
    public void setLastSuccessful(Build value) {
        this.lastSuccessful = value;
    }

    /**
     * Gets the value of the lastFinished property.
     * 
     * @return
     *     possible object is
     *     {@link Build }
     *     
     */
    public Build getLastFinished() {
        return lastFinished;
    }

    /**
     * Sets the value of the lastFinished property.
     * 
     * @param value
     *     allowed object is
     *     {@link Build }
     *     
     */
    public void setLastFinished(Build value) {
        this.lastFinished = value;
    }

}
