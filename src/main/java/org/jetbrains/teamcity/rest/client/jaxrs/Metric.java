
package org.jetbrains.teamcity.rest.client.jaxrs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for metric complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="metric"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}metricValues" minOccurs="0"/&gt;
 *         &lt;element ref="{}metricTags" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="prometheusName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "metric", propOrder = {
    "metricValues",
    "metricTags"
})
public class Metric {

    protected MetricValues metricValues;
    protected MetricTags metricTags;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "description")
    protected String description;
    @XmlAttribute(name = "prometheusName")
    protected String prometheusName;

    /**
     * Gets the value of the metricValues property.
     * 
     * @return
     *     possible object is
     *     {@link MetricValues }
     *     
     */
    public MetricValues getMetricValues() {
        return metricValues;
    }

    /**
     * Sets the value of the metricValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link MetricValues }
     *     
     */
    public void setMetricValues(MetricValues value) {
        this.metricValues = value;
    }

    /**
     * Gets the value of the metricTags property.
     * 
     * @return
     *     possible object is
     *     {@link MetricTags }
     *     
     */
    public MetricTags getMetricTags() {
        return metricTags;
    }

    /**
     * Sets the value of the metricTags property.
     * 
     * @param value
     *     allowed object is
     *     {@link MetricTags }
     *     
     */
    public void setMetricTags(MetricTags value) {
        this.metricTags = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the prometheusName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrometheusName() {
        return prometheusName;
    }

    /**
     * Sets the value of the prometheusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrometheusName(String value) {
        this.prometheusName = value;
    }

}
