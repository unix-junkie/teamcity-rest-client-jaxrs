
package org.jetbrains.teamcity.rest.client.jaxrs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for licensingData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="licensingData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}licenseKeys" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="licenseUseExceeded" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="maxAgents" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="unlimitedAgents" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="agentsLeft" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="maxBuildTypes" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="unlimitedBuildTypes" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="buildTypesLeft" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="serverLicenseType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="serverEffectiveReleaseDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "licensingData", propOrder = {
    "licenseKeys"
})
public class LicensingData {

    protected LicenseKeys licenseKeys;
    @XmlAttribute(name = "licenseUseExceeded")
    protected Boolean licenseUseExceeded;
    @XmlAttribute(name = "maxAgents")
    protected Integer maxAgents;
    @XmlAttribute(name = "unlimitedAgents")
    protected Boolean unlimitedAgents;
    @XmlAttribute(name = "agentsLeft")
    protected Integer agentsLeft;
    @XmlAttribute(name = "maxBuildTypes")
    protected Integer maxBuildTypes;
    @XmlAttribute(name = "unlimitedBuildTypes")
    protected Boolean unlimitedBuildTypes;
    @XmlAttribute(name = "buildTypesLeft")
    protected Integer buildTypesLeft;
    @XmlAttribute(name = "serverLicenseType")
    protected String serverLicenseType;
    @XmlAttribute(name = "serverEffectiveReleaseDate")
    protected String serverEffectiveReleaseDate;

    /**
     * Gets the value of the licenseKeys property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseKeys }
     *     
     */
    public LicenseKeys getLicenseKeys() {
        return licenseKeys;
    }

    /**
     * Sets the value of the licenseKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseKeys }
     *     
     */
    public void setLicenseKeys(LicenseKeys value) {
        this.licenseKeys = value;
    }

    /**
     * Gets the value of the licenseUseExceeded property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLicenseUseExceeded() {
        return licenseUseExceeded;
    }

    /**
     * Sets the value of the licenseUseExceeded property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLicenseUseExceeded(Boolean value) {
        this.licenseUseExceeded = value;
    }

    /**
     * Gets the value of the maxAgents property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxAgents() {
        return maxAgents;
    }

    /**
     * Sets the value of the maxAgents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxAgents(Integer value) {
        this.maxAgents = value;
    }

    /**
     * Gets the value of the unlimitedAgents property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnlimitedAgents() {
        return unlimitedAgents;
    }

    /**
     * Sets the value of the unlimitedAgents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnlimitedAgents(Boolean value) {
        this.unlimitedAgents = value;
    }

    /**
     * Gets the value of the agentsLeft property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAgentsLeft() {
        return agentsLeft;
    }

    /**
     * Sets the value of the agentsLeft property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAgentsLeft(Integer value) {
        this.agentsLeft = value;
    }

    /**
     * Gets the value of the maxBuildTypes property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxBuildTypes() {
        return maxBuildTypes;
    }

    /**
     * Sets the value of the maxBuildTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxBuildTypes(Integer value) {
        this.maxBuildTypes = value;
    }

    /**
     * Gets the value of the unlimitedBuildTypes property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnlimitedBuildTypes() {
        return unlimitedBuildTypes;
    }

    /**
     * Sets the value of the unlimitedBuildTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnlimitedBuildTypes(Boolean value) {
        this.unlimitedBuildTypes = value;
    }

    /**
     * Gets the value of the buildTypesLeft property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBuildTypesLeft() {
        return buildTypesLeft;
    }

    /**
     * Sets the value of the buildTypesLeft property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBuildTypesLeft(Integer value) {
        this.buildTypesLeft = value;
    }

    /**
     * Gets the value of the serverLicenseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerLicenseType() {
        return serverLicenseType;
    }

    /**
     * Sets the value of the serverLicenseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerLicenseType(String value) {
        this.serverLicenseType = value;
    }

    /**
     * Gets the value of the serverEffectiveReleaseDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerEffectiveReleaseDate() {
        return serverEffectiveReleaseDate;
    }

    /**
     * Sets the value of the serverEffectiveReleaseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerEffectiveReleaseDate(String value) {
        this.serverEffectiveReleaseDate = value;
    }

}
