
package org.jetbrains.teamcity.rest.client.jaxrs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for related complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="related"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}builds" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "related", propOrder = {
    "builds"
})
public class Related {

    protected Builds builds;

    /**
     * Gets the value of the builds property.
     * 
     * @return
     *     possible object is
     *     {@link Builds }
     *     
     */
    public Builds getBuilds() {
        return builds;
    }

    /**
     * Sets the value of the builds property.
     * 
     * @param value
     *     allowed object is
     *     {@link Builds }
     *     
     */
    public void setBuilds(Builds value) {
        this.builds = value;
    }

}
