#!/usr/bin/make -f

TARGET_PACKAGE = org.jetbrains.teamcity.rest.client.jaxrs

ifeq ($(WADL2JAVA),)
WADL2JAVA := wadl2java
endif

BASEDIR = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))
SRCDIR = $(BASEDIR)/src/main/java
SCRIPT_DIR = $(BASEDIR)/src/main/scripts

.PHONY: all
all: $(BASEDIR)/application.wadl $(BASEDIR)/xsd1.xsd
	$(WADL2JAVA) -o "$(SRCDIR)" -s jaxrs20 -p $(TARGET_PACKAGE) $<
	find "$(SRCDIR)" -type f -name '*\.java' -exec sed -E -i'.orig' -f "$(SCRIPT_DIR)/post-generate.sed" '{}' ';'
	find "$(SRCDIR)" -type f -name '*\.java\.orig' -delete

.PHONY: clean
clean:
	$(RM) -r $(SRCDIR)/*
