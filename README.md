TeamCity REST API Client, JAX-RS 2.0 flavour
============================================

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build Status](https://travis-ci.org/unix-junkie/teamcity-rest-client-jaxrs.svg?branch=master)](https://travis-ci.org/unix-junkie/teamcity-rest-client-jaxrs)
[![Build Status](https://gitlab.com/unix-junkie/teamcity-rest-client-jaxrs/badges/master/pipeline.svg)](https://gitlab.com/unix-junkie/teamcity-rest-client-jaxrs)
[![Download](https://api.bintray.com/packages/unix-junkie/maven/teamcity-rest-client-jaxrs/images/download.svg)](https://bintray.com/unix-junkie/maven/teamcity-rest-client-jaxrs)
[![JitPack](https://jitpack.io/v/unix-junkie/teamcity-rest-client-jaxrs.svg)](https://jitpack.io/#unix-junkie/teamcity-rest-client-jaxrs)

Code generation instructions
----------------------------

1. Make sure you've built the latest `wadl2java` tool from https://github.com/javaee/wadl.
   The same tool from [_Apache CXF_](http://cxf.apache.org/docs/jaxrs-services-description.html#JAXRSServicesDescription-wadl2javacommandlinetool)
   produces uncompilable code and thus can't be used.
1. Download [`application.wadl`](http://teamcity:8111/app/rest/application.wadl)
   as well as [`xsd1.xsd`](http://teamcity:8111/app/rest/application.wadl/xsd1.xsd)
   from your _TeamCity_ instance and save both files locally.
1. Edit both `application.wadl` and `xsd1.xsd` so that these conditions are met:
    1. neither http://cctray namespace nor any types from it are referenced:
       ```diff
       --- xsd1.xsd.orig	2019-10-01 19:56:57.000000000 +0300
       +++ xsd1.xsd	2019-10-01 19:58:18.839904039 +0300
       @@ -1,9 +1,5 @@
        <?xml version="1.0" standalone="yes"?>
       -<xs:schema version="1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ns1="http://cctray">
       -
       -  <xs:import namespace="http://cctray" schemaLocation="xsd0.xsd"/>
       -
       -  <xs:element name="Projects" type="ns1:projects"/>
       +<xs:schema version="1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">

          <xs:element name="VcsCheckStatus" type="VcsCheckStatus"/>

       ```
       Modify `application.wadl` accordingly:
       ```diff
       --- application.wadl.orig	2019-10-01 20:04:21.327544109 +0300
       +++ application.wadl	2019-10-01 20:04:48.143813416 +0300
       @@ -8,9 +8,6 @@
                <include href="application.wadl/xsd1.xsd">
                    <doc title="Generated" xml:lang="en"/>
                </include>
       -        <include href="application.wadl/xsd0.xsd">
       -            <doc title="Generated" xml:lang="en"/>
       -        </include>
            </grammars>
            <resources base="http://localhost:8112/bs/">
                <resource path="/app/rest/2018.1/agentPools">

       ```
    1. `application.wadl` includes `xsd1.xsd` from a valid local path:
       ```diff
       --- application.wadl.orig	2019-10-01 20:05:51.560450295 +0300
       +++ application.wadl	2019-10-01 20:06:03.736572579 +0300
       @@ -5,7 +5,7 @@
            See also https://www.jetbrains.com/help/teamcity/?REST+API
          </doc>
            <grammars>
       -        <include href="application.wadl/xsd1.xsd">
       +        <include href="xsd1.xsd">
                    <doc title="Generated" xml:lang="en"/>
                </include>
            </grammars>
       ```
1. Modify the original URL in `application.wadl` (will affect the name of one of
the generated classes):
   ```diff
   --- application.wadl.orig	2019-10-01 20:11:39.147941294 +0300
   +++ application.wadl	2019-10-01 20:13:27.909033720 +0300
   @@ -9,7 +9,7 @@
                <doc title="Generated" xml:lang="en"/>
            </include>
        </grammars>
   -    <resources base="http://localhost:8112/bs/">
   +    <resources base="http://ApiClient/">
            <resource path="/app/rest/2018.1/agentPools">
                <method id="createPool" name="POST">
                    <request>
   ```
1. Generate the source code:
   ```bash
   wadl2java -o . -s jaxrs20 -p org.jetbrains.teamcity.rest.client.jaxrs application.wadl
   ```
   The resulting entry point class will be
   `org.jetbrains.teamcity.rest.client.jaxrs.ApiClient`.

Optional dependencies
---------------------

1. If you're receiving `java.lang.IllegalStateException: InjectionManagerFactory not found`
   at run time (which will happen unless you're running within a container):
   ```xml
   <dependency>
   	<groupId>org.glassfish.jersey.inject</groupId>
   	<artifactId>jersey-hk2</artifactId>
   	<version>${jersey.version}</version>
   	<scope>runtime</scope>
   </dependency>
   ```
1. If you need _OAuth 2.0_ support (for
   [Token-Based Authentication](https://www.jetbrains.com/help/teamcity/configuring-authentication-settings.html#Token-Based-Authentication)):
   ```xml
   <dependency>
   	<groupId>org.glassfish.jersey.security</groupId>
   	<artifactId>oauth2-client</artifactId>
   	<version>${jersey.version}</version>
   </dependency>
   ```
1. If you need _NTLM_ authentication support:
   ```xml
   <dependency>
   	<groupId>org.glassfish.jersey.connectors</groupId>
   	<artifactId>jersey-apache-connector</artifactId>
   	<version>${jersey.version}</version>
   </dependency>
   ```
1. If you need dynamic proxies, e.g.:
   ```kotlin
   @Path("/app/rest/runningBuilds")
   interface TeamCityRestApi {
   	@GET
   	@Path("/test")
   	@Produces(TEXT_PLAIN)
   	fun test(): String

   	@POST
   	@Path("/test")
   	@Produces(TEXT_PLAIN)
   	fun testPost(): String
   }

   // ...

   val client: Client = ClientBuilder.newClient()

   // ...

   val restApi: TeamCityRestApi = WebResourceFactory.newResource(TeamCityRestApi::class.java, client.target(baseUri))
   ```
   then you also need to include
   ```xml
   <dependency>
   	<groupId>org.glassfish.jersey.ext</groupId>
   	<artifactId>jersey-proxy-client</artifactId>
   	<version>${jersey.version}</version>
   </dependency>
   ```
1. If you need automatic un-marshalling from XML:
   ```xml
   <dependency>
   	<groupId>org.glassfish.jersey.media</groupId>
   	<artifactId>jersey-media-jaxb</artifactId>
   	<version>${jersey.version}</version>
   	<scope>runtime</scope>
   </dependency>
   ```
1. If you need automatic un-marshalling from JSON:
   ```xml
   <dependency>
   	<groupId>org.glassfish.jersey.media</groupId>
   	<artifactId>jersey-media-json-jackson</artifactId>
   	<version>${jersey.version}</version>
   	<scope>runtime</scope>
   </dependency>
   ```

JAXB and Java 1.9+
------------------

The library includes [JAXB](https://github.com/eclipse-ee4j/jaxb-ri) for Java 1.9+
using a [_Maven profile_](https://maven.apache.org/guides/introduction/introduction-to-profiles.html)
with conditional activation. This is fine as long as clients use _Maven_, too.
For non-_Maven_ clients, you'll need to include:

 * [`javax.xml.bind:jaxb-api`](https://search.maven.org/search?q=g:javax.xml.bind%20AND%20a:jaxb-api&core=gav)
 * [`com.sun.xml.bind:jaxb-core`](https://search.maven.org/search?q=g:com.sun.xml.bind%20AND%20a:jaxb-core&core=gav)
 * [`com.sun.xml.bind:jaxb-impl`](https://search.maven.org/search?q=g:com.sun.xml.bind%20AND%20a:jaxb-impl&core=gav)
 * [`javax.activation:activation`](https://search.maven.org/search?q=g:javax.activation%20AND%20a:activation&core=gav)

For _Gradle_, this can be implemented as follows:

```kotlin
dependencies {
    if (JavaVersion.current() > JavaVersion.VERSION_1_8) {
        implementation("javax.xml.bind:jaxb-api:2.3.1")
        implementation("com.sun.xml.bind:jaxb-core:2.3.0.1")
        implementation("com.sun.xml.bind:jaxb-impl:2.3.2")
        implementation("javax.activation:activation:1.1.1")
    }
}
```
